CC := gcc
CFLAGS := -Wall -fPIC
LDFLAGS := -shared

INCDIR := include
SRCDIR := src
OBJDIR := build
LIBDIR := lib

INCS := $(shell find $(SRCDIR) -name "*.h")
SRCS := $(shell find $(SRCDIR) -name "*.c")
OBJS := $(patsubst $(SRCDIR)/%.c, $(OBJDIR)/%.o, $(SRCS))
LIBS := $(patsubst $(OBJDIR)/%.o, $(LIBDIR)/$(LIBDIR)%.so, $(OBJS))

MKDIR := mkdir -p
RM := rm
RMDIR := $(RM) -rf

SUDO := sudo
CP := cp

SYSPATH := /usr/local

all: build

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	@$(MKDIR) $(OBJDIR)
	$(CC) $(CFLAGS) -c $< -o $@

$(LIBDIR)/lib%.so: $(OBJDIR)/%.o
	@$(MKDIR) $(LIBDIR)
	$(CC) $(LDFLAGS) $^ -o $@

build: $(OBJS) $(LIBS)

install: build
	$(SUDO) $(CP) $(INCS) $(SYSPATH)/$(INCDIR)/
	$(SUDO) $(CP)$(LIBS) $(SYSPATH)/$(LIBDIR)/

uninstall:
	$(SUDO) $(RM) $(SYSPATH)/$(INCDIR)/$(notdir $(INCS))
	$(SUDO) $(RM) $(SYSPATH)/$(LIBDIR)/$(notdir $(LIBS))

clean:
	$(RMDIR) $(OBJDIR) $(LIBDIR)
