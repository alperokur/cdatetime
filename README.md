# Datetime Library

## Overview
This project provides a static library for managing date and time in C. It includes functionalities for creating and manipulating date and datetime objects, converting between different formats, and obtaining the current date and time.

## Features
- Creation and manipulation of date and datetime objects.
- Conversion between date and datetime structures.
- String conversion functions to and from date and datetime.
- Validation of dates and times.

## Installation

### Prerequisites
- GCC (GNU Compiler Collection)
- Make utility

## Building the Library
To build the library, navigate to the project directory and run `make`. This will compile the source files and create the necessary object files and shared libraries in the `build` and `lib` directories, respectively.

### Installing the Library
To install the library and its headers to your system, run `sudo make install`. This command will copy the header files to `/usr/include` and the shared libraries to `/usr/lib`.

### Uninstalling the Library
To uninstall the library and its headers from your system, run `sudo make uninstall`. This will remove the installed files from `/usr/include` and `/usr/lib`.

### Cleaning Up
To clean up the build artifacts, run `make clean`. This command will remove the `build` and `lib` directories along with their contents.

## Usage

### Include the Header
Include the main header file in your C source files:

    #include <datetime.h>

### Linking the Library

When compiling your project, link against the datetime library using `-ldatetime`. For example:
    
    gcc -o myprogram myprogram.c -ldatetime
    
## License

This project is licensed under the GNU General Public License v3.0. See the LICENSE file for more details.
