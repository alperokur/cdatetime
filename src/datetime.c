/* datetime.c
 *
 * Copyright 2024 Alper Okur
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "datetime.h"

static int is_valid_year(int year) {
  return year >= MIN_YEAR && year <= MAX_YEAR;
}

static int is_valid_month(int month) {
  return month >= MIN_MONTH && month <= MAX_MONTH;
}

static int is_leap_year(int year) {
  return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
}

static int max_day_in_month(int year, int month) {
  if (!is_valid_year(year) || !is_valid_month(month))
    return -1;

  switch (month) {
    case 2:
      return is_leap_year(year) ? MAX_DAY - 2 : MAX_DAY - 3;
    case 4:
    case 6:
    case 9:
    case 11:
      return MAX_DAY - 1;
  }

  return MAX_DAY;
}

static int is_valid_day(int year, int month, int day) {
  return day >= MIN_DAY && day <= max_day_in_month(year, month);
}

static int is_valid_hour(int hour) {
  return hour >= MIN_HOUR && hour <= MAX_HOUR;
}

static int is_valid_minute(int minute) {
  return minute >= MIN_MINUTE && minute <= MAX_MINUTE;
}

static int is_valid_second(int second) {
  return second >= MIN_SECOND && second <= MAX_SECOND;
}

date *new_date(int year, int month, int day) {
  if (!is_valid_day(year, month, day))
    return NULL;

  date *d = (date *)malloc(sizeof(date));
  if (d == NULL) {
    fprintf(stderr, 
            "Error: Memory allocation failed for 'date'." 
            "Unable to allocated %zu bytes. \n", sizeof(date));
    return NULL;
  }

  d->year = year;
  d->month = month;
  d->day = day;

  return d;
}

datetime *new_datetime(int year, int month, int day,
                       int hour, int minute, int second) {
  if (!is_valid_hour(hour) || !is_valid_minute(minute) || 
      !is_valid_second(second))
    return NULL;

  date *d = new_date(year, month, day);
  if (d == NULL) {
    return NULL;
  }
  
  datetime *dt = (datetime *)malloc(sizeof(datetime));
  if (dt == NULL) {
    fprintf(stderr, 
            "Error: Memory allocation failed for 'datetime'." 
            "Unable to allocated %zu bytes. \n", sizeof(datetime));
    return NULL;
  }

  dt->d = d;
  dt->hour = hour;
  dt->minute = minute;
  dt->second = second;

  return dt;
}

void free_date(date *d) {
  free(d);
}

void free_datetime(datetime *dt) {
  free_date(dt->d);
  free(dt);
}

datetime *now() {
  time_t now;
  time(&now);
  struct tm *local = localtime(&now);
  return new_datetime(local->tm_year + 1900, local->tm_mon + 1, local->tm_mday, 
                      local->tm_hour, local->tm_min, local->tm_sec);
}

datetime *date_to_datetime(date *d) {
  return new_datetime(d->year, d->month, d->day, 0, 0, 0);
}

date *datetime_to_date(datetime *dt) {
  return dt->d;
}

char *date_to_string(date *d) {
  size_t format_length = strlen("yyyy-MM-dd") + 1;
  char *str = (char *)malloc(format_length);
  
  if (str == NULL) {
    fprintf(stderr, 
            "Error: Memory allocation failed for 'date_string'. " 
            "Unable to allocate %zu bytes.\n", format_length);
    return NULL; 
  }

  snprintf(str, format_length, DATE_FORMAT,
           d->year, d->month, d->day);

  return str;
}

char *datetime_to_string(datetime *dt) {
  size_t format_length = strlen("yyyy-MM-dd hh:mm:ss") + 1;
  char *str = (char *)malloc(format_length);
  
  if (str == NULL) {
    fprintf(stderr, 
            "Error: Memory allocation failed for 'datetime_string'. " 
            "Unable to allocate %zu bytes.\n", format_length);
    return NULL; 
  }

  snprintf(str, format_length, DATETIME_FORMAT,
           dt->d->year, dt->d->month, dt->d->day,
           dt->hour, dt->minute, dt->second);

  return str;
}

date *string_to_date(const char *str) {
  date *d = (date *)malloc(sizeof(date));
  if (d == NULL) {
    fprintf(stderr, 
            "Error: Memory allocation failed for 'date'. " 
            "Unable to allocate %zu bytes.\n", sizeof(date));
    return NULL;
  }

  sscanf(str, DATE_FORMAT,
         &d->year, &d->month, &d->day);

  return d;
}

datetime *string_to_datetime(const char *str) {
  datetime *dt = (datetime *)malloc(sizeof(datetime));
  if (dt == NULL) {
    fprintf(stderr,
            "Error: Memory allocation failed for 'datetime'. "
            "Unable to allocate %zu bytes.\n", sizeof(datetime));
    return NULL;
  }

  dt->d = (date *)malloc(sizeof(date));
  if (dt->d == NULL) {
    fprintf(stderr, 
            "Error: Memory allocation failed for 'date'. " 
            "Unable to allocate %zu bytes.\n", sizeof(date));
    free(dt);
    return NULL;
  }

  sscanf(str, DATETIME_FORMAT,
         &dt->d->year, &dt->d->month, &dt->d->day,
         &dt->hour, &dt->minute, &dt->second);

  return dt;
}
