/* datetime.h
 *
 * Copyright 2024 Alper Okur
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef DATETIME_H
#define DATETIME_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MIN_YEAR 1900
#define MAX_YEAR 2100

#define MIN_MONTH 1
#define MAX_MONTH 12

#define MIN_DAY 1
#define MAX_DAY 31

#define MIN_HOUR 0
#define MAX_HOUR 23

#define MIN_MINUTE 0
#define MAX_MINUTE 59

#define MIN_SECOND 0
#define MAX_SECOND 59

#define DATE_FORMAT "%04d-%02d-%02d"
#define DATETIME_FORMAT DATE_FORMAT " %02d:%02d:%02d"

typedef struct {
  int year;
  int month;
  int day;
} date;

typedef struct {
  date *d;
  int hour;
  int minute;
  int second;
} datetime;

date *new_date(int year, int month, int day);
datetime *new_datetime(int year, int month, int day,
                       int hour, int minute, int second);

void free_date(date *d);
void free_datetime(datetime *dt);

datetime *now();

datetime *date_to_datetime(date *d);
date *datetime_to_date(datetime *dt);

char *date_to_string(date *d);
char *datetime_to_string(datetime *dt);

date *string_to_date(const char *str);
datetime *string_to_datetime(const char *str);

#endif // DATETIME_H
